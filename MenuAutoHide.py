import sublime
import sublime_plugin

class MenuAutoHide(sublime_plugin.EventListener):
	SETTINGS = 'MenuAutoHide.sublime-settings'
	__settings = {}

	def __init__(self):
		MenuAutoHide.settings_update()

		self.__settings.clear_on_change('settings')
		self.__settings.add_on_change(
			'ignore',
			self.settings_update
		)

	def on_text_command(self, view, command_name, args):
		self.hide_menu(view.window())

	def on_window_command(self, window, command_name, args):
		if (command_name not in (self.settings.get('ignore') or [])):
			self.hide_menu(window)

	def hide_menu(self, window):
		if window.is_menu_visible():
			window.set_menu_visible(False)

		cmd = self.settings.get('after_hide')
		if (cmd):
			window.run_command(cmd);

	def settings_update():
		MenuAutoHide.__settings = sublime \
			.load_settings(MenuAutoHide.SETTINGS)

	@property
	def settings(self):
		if (not self.__settings):
			MenuAutoHide.settings_update()

		return self.__settings

def plugin_loaded():
	MenuAutoHide.settings_update()
